import math


def hypotenuse(a, b):
    c = (a * a) + (b * b)
    answer = math.sqrt(c)
    return answer


print("This is a hypotenuse calculater")
a = int(input("a: "))
b = int(input("b: "))
h = hypotenuse(a, b)
print(f"The hypotenuse of {a} and {b} is {h}")
