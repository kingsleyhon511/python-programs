# set K,Q,J = 10/done
# set A = 1,11/done
# set a bust limit, 21/done
# add numbers/done
# make blackjack, 10 and A/done
# check to see if player (or dealer) has blackjack/done
# make dealer show covered card/done
# let the player hit or stay/done
# make dealer hit until 17 or over/done
# split
# add players
# money
# make a new deck
# determine if win or lose/done

import os
import random


class Game:
    def __init__(self, deck, dealer, player):
        self.deck = deck
        self.dealer = dealer
        self.player = player

    def start(self):
        self.deck.shuffle()
        # print(deck)
        # print("-------------")
        self.player.draw(self.deck.pass_out())
        self.player.draw(self.deck.pass_out())
        self.dealer.draw(self.deck.pass_out())
        self.dealer.draw(self.deck.pass_out())
        # self.player.draw(Card("Spades", 4))
        # self.player.draw(Card("Hearts", 6))
        # self.dealer.draw(Card("Hearts", 10))
        # self.dealer.draw(Card("Clubs", 10))

        print(f"player's total: {self.player.get_total_value()}")
        if self.player.is_blackjack():
            print(f"{self.player.name} got a Blackjack!")
        else:
            print("no blackjack for player")
        # print(f"dealer's total: {self.dealer.get_total_value()}")
        if self.dealer.is_blackjack():
            print(f"{self.dealer.name} got a Blackjack!")
        else:
            print("no blackjack for dealer")
        # print(deck)
        # print(deck)
        # print("-------------")
        print(f"{self.player.name} cards: ")
        self.player.show_hand()
        print("-------------")
        print("dealer's cards:")
        self.dealer.show_hand()
        #self.dealer.discard_all()
        # os.system('clear')
        #self.dealer.show_hand()
        #self.player.win()
        # self.player.win()
        # player playing
        hit = "y"
        if self.has_blackjack():
            hit = "n"
        while hit == "y" and not self.player.bust():
            hit = input("do you want to hit(y/n)?: ")
            if hit == "y":
                self.player.draw(self.deck.pass_out())
                self.player.show_hand()
                print(f"player's total: {self.player.get_total_value()}")
                if self.player.bust():
                    print("Game Over")

        # dealer playing
        while self.dealer.get_total_value() < 17 and not self.player.bust():
            self.dealer.draw(self.deck.pass_out())
            if self.dealer.bust():
                print(f"{self.player.name} won!")
        print(f"dealer's total: {self.dealer.get_total_value()}")
        self.dealer.show_all_hand()

        # print result
        self.print_results()


    def has_blackjack(self):
        end_game = False
        if self.player.is_blackjack() and not self.dealer.is_blackjack():
            self.player.has_won(self.dealer.get_total_value())
            print(f"{self.player.name} won!")
            end_game = True
        elif self.dealer.is_blackjack() and not self.player.is_blackjack():
            self.dealer.has_won(self.player.get_total_value())
            print(f"{self.dealer.name} won!")
            end_game = True
        elif self.player.is_blackjack() and self.dealer.is_blackjack():
            print("b_push")
            end_game = True
        return end_game
        
    def has_player_won(self):
        player_won = False
        if (not self.player.bust() and (self.player.get_total_value() > self.dealer.get_total_value()) 
            or self.dealer.bust()):
            player_won = True
        return player_won

    def has_dealer_won(self):
        dealer_won = False
        if (not self.dealer.bust() and (self.dealer.get_total_value() > self.player.get_total_value()) 
            or self.player.bust()):
            dealer_won = True
        return dealer_won

    def print_results(self):
        if self.has_player_won():
            print(f"{self.player.name} won!")
        elif self.has_dealer_won():
            print(f"{self.dealer.name} won!")
        else:
            print("r_push")

    # def hit_or_stay(self, player, card):

        """discard = input("do you want to discard any cards(y,n)?: ")
        if discard == "y":
            discard_suit = input("what is the suit of card you want to discard?:")
            discard_number = int(input("what is the number of the card you want to discard?: "))
            discard_card = Card(discard_suit, discard_number)
            player1.discard(discard_card)
        player1.show_hand()"""


class Card:
    def __init__(self, suit, number):
        self.suit = suit
        self.number = number
        self.value = self.number
        if self.number > 10:
            self.value = 10
        elif self.number == 1:
            self.value = 11

    def __str__(self):
        if self.suit == "Spades":
            suit = "\u2660"
        elif self.suit == "Hearts":
            suit = "\u2665"
        elif self.suit == "Clubs":
            suit = "\u2663"
        elif self.suit == "Diamonds":
            suit = "\u2666"
        if self.number == 1:
            number = "A"
        elif self.number == 11:
            number = "J"
        elif self.number == 12:
            number = "Q"
        elif self.number == 13:
            number = "K"
        else:
            number = self.number
        return f"{number} of {suit}"

    def __add__(self, other):
        return str(self) + other

    def __radd__(self, other):
        return other + str(self)

    def __eq__(self, other):
        return self.suit == other.suit and self.number == other.number


class Deck8:
    def __init__(self):
        self.cards = []
        self.gen_cards()

    def gen_cards(self):
        for d8 in range(1, 9):
            for suit in ['Spades', 'Hearts', 'Clubs', 'Diamonds']:
                for i in range(1, 14):
                    card = Card(suit, i)
                    self.cards.append(card)

    def __str__(self):
        deck_of_cards = ""
        for c in self.cards:
            deck_of_cards += c + "\n"
        return deck_of_cards

    def shuffle(self):
        for rand in range(len(self.cards) - 1):
            rand_card = random.randint(1, (len(self.cards) - 1))
            self.cards[rand], self.cards[rand_card] = self.cards[rand_card], self.cards[rand]

    def pass_out(self):
        # print("Deck.pass_out")
        return self.cards.pop(0)


class Player:
    def __init__(self, name):
        self.hand = []
        self.name = name

    def draw(self, card):
        # print("player.draw")
        self.hand.append(card)

    def get_total_value(self):
        hand_total = 0
        for h in self.hand:
            hand_total += h.value
        for h in self.hand:
            if hand_total > 21:
                if h.number == 1 and h.value == 11:
                    h.value = 1
                    return self.get_total_value()
        return hand_total

    def discard(self, card):
        for x in range(len(self.hand)):
            print(f"card:{card}, self.hand: {self.hand[x]}")
            if card == self.hand[x]:
                # print("are_cards_equal")
                self.hand.pop(x)
                break

    def discard_all(self):
        while len(self.hand) > 0:
            self.hand.pop()

    def show_hand(self):
        for h in self.hand:
            print(h)

    def is_blackjack(self):
        hand_total = self.get_total_value()
        if hand_total == 21 and len(self.hand) == 2:
            return True
        else:
            return False

    def hit(self, card):
        if self.get_total_value() <= 21:
            self.draw(card)

    def bust(self):
        return self.get_total_value() > 21

    def has_won(self, dealers_total):
        won = False
        if self.get_total_value() > dealers_total:
           won = True
        return won

class Dealer(Player):
    # hide card
    def show_hand(self):
        if (len(self.hand) > 0):
            print(f"covered card")
            for h in range(1, len(self.hand)):
                print(self.hand[h])

    def show_all_hand(self):
        for h in self.hand:
            print(h)

player = Player("Kingsley")
dealer = Dealer("Dealer")
deck = Deck8()
game = Game(deck, dealer, player)
game.start()
