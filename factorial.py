def fact(f):
    product = 1
    for factor in range(f,0,-1):
        product *= factor
    return product   
f = 10
factorial = fact(f)
print(factorial)