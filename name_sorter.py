def fam(family):
    for compare in range(len(family)-1,0,-1):
        for sort in range(compare):
            if family[sort] < family[sort + 1]:
                family[sort], family[sort + 1] = family[sort + 1], family[sort]
    return family
family = ['elephant','giraffe','zebra','cat', 'fish']
name_list = fam(family)
print(name_list)