import random

def rand_list(my_list):
    for i in range(len(my_list) - 1):
        rand_num = random.randint(1, (len(my_list) - 1))
        my_list[i], my_list[rand_num] = my_list[rand_num], my_list[i]
    return my_list 

my_list = [1, 2, 3, 4, 5]
shuffle = rand_list(my_list) 
print(shuffle)